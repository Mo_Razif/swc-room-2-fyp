# 

<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->

<a name="readme-top"></a>

<!--

- ** Thanks for checking out the Best-README-Template. If you have a suggestion
- ** that would make this better, please fork the repo and create a pull request
- ** or simply open an issue with the tag "enhancement".
- ** Don't forget to give the project a star!
- ** Thanks again! Now go create something AMAZING! :D
- ->

<!-- PROJECT SHIELDS -->

<!--

- ** I'm using markdown "reference style" links for readability.
- ** Reference links are enclosed in brackets instead of parentheses ( ).
- ** See the bottom of this document for the declaration of the reference variables
- ** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
- ** https://www.markdownguide.org/basic-syntax/#reference-style-links
- ->

[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->


<h3 align="center">Final Project SWC2 - 2024</h3>

<!-- TABLE OF CONTENTS -->

<details>

<summary>Table of Contents</summary>

<ol>

<li>

<a href="#about-the-project">About The Project</a>

<ul>

<li><a href="#built-with">Built With</a></li>

</ul>

</li>

<li><a href="#usage">Usage</a></li>

<li><a href="#collaborating">Collaborating</a></li>

<li><a href="#license">License</a></li>

<li><a href="#contact">Contact</a></li>

<li><a href="#acknowledgments">Acknowledgments</a></li>

</ol>

</details>

<!-- ABOUT THE PROJECT -->

## About The Project

Final Project SWC2 - 2024

- Final Project SWC2 - 2024

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

This section should list any major frameworks/libraries used to bootstrap your project. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.

- [![Laravel][nodejs]][node-url]
- [![Livewire][mongodb]][mongodb-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

*_For more examples, please refer to the [Documentation](https://youtube.com)_*

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Collaborating

Any contributions you make are ****greatly appreciated****.


1. The latest branch is (`develop`) which is for the staging release

2. The production branch is (`main`) which is for the production release

3. Create your Feature Branch (`git checkout -b feature/ticketing-number`) from (`develop`)

4. Create your hotfix Branch (`git checkout -b feature/ticketing-number`) from (`main`)

5. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)

6. Push to the Branch (`git push origin feature/AmazingFeature`)

7. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

Use this space to list resources you find helpful and would like to give credit to. I've included a few of my favorites to kick things off!

- [Node JS](https://laravel.com)
- [Mongo DB](https://laravel-livewire.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[nodejs]: https://img.shields.io/badge/NODE%20JS-5FA04E?style=for-the-badge&logo=node.js&logoColor=white

[node-url]: https://nodejs.org/en

[mongodb]: https://img.shields.io/badge/MONGO%20DB-47A248?style=for-the-badge&logo=mongodb&logoColor=white

[mongodb-url]: https://www.mongodb.com/
